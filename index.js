var async = require('async')
var bluebird = require('bluebird')
var listFilesInFolder = require('./list.files.in.folder')
var listSheetsInFile = require('./list.sheets.in.file')
var readValuesInSheet = require('./read.values.in.sheet')

module.exports = function (auth) {
    return {
        readFolder: readFolder
    }

    function readFolder(folderId) {
        var acc = {
            folderId: folderId
        }
        /*
            Accumulator is expected to look like
            {
                auth:auth,
                folderId:___,
                files:[fileId1, fileId2],
                sheets:[{
                    sheetId:___,
                    fileId:____,
                    rowWise:{}
                },{
                    sheetId:___,
                    fileId:____,
                    rowWise:{}
                },...]

            }
        */
        console.log('calling attachFiles')
        return attachFiles(acc)
            .then(function (acc_files_attached) {
                console.log('Calling attachSheets')
                return attachSheets(acc_files_attached)
            }).then(function (acc_sheets_attached) {
                console.log('Calling attachValues')
                return attachValues(acc_sheets_attached)
            })
    }

    function attachFiles(acc) {
        console.log('calling listFilesInFolder ')
        return listFilesInFolder(acc.folderId, auth)
            .then(function (files) {
                return createPromise(acc, 'files', files)
            })
    }

    function attachSheets(acc) {
        var D = bluebird.pending()
        acc.sheets = []
        async.mapSeries(acc.files, function (file, callback) {

            listSheetsInFile(file.id, auth)
                .then(function (sheets) {
                    sheets.map(function (sheet) {
                        sheet.fileId = file.id
                        acc.sheets.push(sheet)
                    })
                    callback(null, sheets)
                }).catch(function (err) {
                    console.log('Error in calling listSheetsInFile ', err)
                    callback(err)
                })

        }, function (err) {
            if (err) {

                D.reject(err)
            } else {

                D.resolve(acc)
            }

        })

        return D.promise
    }

    function attachValues(acc) {
        var D = bluebird.defer()
        acc.values = []
        async.mapSeries(acc.sheets, function (sheet, sheet_callback) {

            readValuesInSheet(sheet.properties.title, sheet.fileId, auth)
                .then(function (colWise) {
                    acc.values.push({
                        fileId: sheet.fileId,
                        sheetId: sheet.properties.id,
                        sheetTitle: sheet.properties.title,
                        colWise: colWise
                    })

                    sheet_callback(null, sheet)
                }).catch(function (err) {
                    console.log('Error while calling readValuesInSheet ', err)
                    callback(err)
                })
        }, function (err) {
            if (err) {
                console.log('Error in mapSeries for readValuesInSheet ', err)
                D.reject(err)
            } else {
                D.resolve(acc)
            }
        })

        return D.promise
    }

    function createPromise(acc, key, value) {
        var json = {}
        json[key] = value
        return bluebird.resolve(Object.assign(acc, json))
    }
    function readSpreadSheet(spreadsheetId) {
        /*  
            1. Get all sheets inside spreadsheet
            2. Read each sheet
        */
    }
}