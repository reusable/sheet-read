const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const util = require('util')
// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly', 'https://www.googleapis.com/auth/drive.metadata.readonly'];
const TOKEN_PATH = 'token.json';

// Load client secrets from a local file.
fs.readFile('credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Google Sheets API.
    //authorize(JSON.parse(content), listMajors);
    authorize(JSON.parse(content), listFiles);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error while trying to retrieve access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

/**
 * Prints the names and majors of students in a sample spreadsheet:
 * @see https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function listMajors(auth) {
    const sheets = google.sheets({ version: 'v4', auth });


    sheets.spreadsheets.values.get({
        //spreadsheetId: '1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms',
        spreadsheetId: '1BXU7vPeXEJMZ0iulsTwVQf54b2IllJfx4osoQ5ow14Y',
        range: 'Master',
    }, (err, res) => {
        if (err) return console.log('The API returned an error: ' + err);
        const rows = res.data.values;
        if (rows.length) {
            console.log('Model, Formats:');
            // Print columns A and E, which correspond to indices 0 and 4.
            rows.map((row) => {
                console.log(`${row[0]}, ${row[1]}`);
            });
        } else {
            console.log('No data found.');
        }
    });


    // sheets.spreadsheets.get({
    //     spreadsheetId: '1BXU7vPeXEJMZ0iulsTwVQf54b2IllJfx4osoQ5ow14Y'
    // }, (err, res) => {
    //     if (err) return console.log('The API returned an error: ' + err);
    //     console.log('received data ', res.data)
    //     console.log('Sheets ', util.inspect(res.data.sheets))
    // })
}

function listFiles(auth) {

    //var folderId = '1NhvV_NXRStwg6MVlZa1ASZoFuLevCwuI'
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter folder to read: ', (folderId) => {
        rl.close();
        // var listFilesInFolder = require('./list.files.in.folder')
        // listFilesInFolder(folderId, auth)
        //     .then(function (files) {
        //         console.log('file list \n', files)
        //     })
        var sheet_reader = require('./index')(auth)

        sheet_reader.readFolder(folderId)
            .then(function (read_response) {
                console.log('response of sheet read ', JSON.stringify(read_response))
            })

        // const drive = google.drive({ version: 'v3', auth });
        // drive.files.list({
        //     'pageSize': 10,
        //     'fields': "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
        //     'q': "'" + folderId + "' in parents"
        // }, (err, res) => {
        //     if (err) return console.log('The API returned an error: ' + err);
        //     const files = res.data.files;
        //     console.log(files)
        // });
    })

}