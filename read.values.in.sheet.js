var bluebird = require('bluebird')
const { google } = require('googleapis');

module.exports = function (sheetTitle, fileId, auth) {

    console.log('Inside readValuesInSheet for ', sheetTitle, ' in ', fileId)
    var D = bluebird.pending()

    const sheets = google.sheets({ version: 'v4', auth });

    sheets.spreadsheets.values.get({
        spreadsheetId: fileId,
        range: sheetTitle,
        majorDimension: 'COLUMNS'
    }, (err, res) => {
        if (err) {
            D.reject(err)
        } else {
            D.resolve(res.data.values)
        }
    })

    return D.promise
}