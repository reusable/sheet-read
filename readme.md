Module to read values from a folder in a spreadsheet

Usage:

var sheet_read = require('sheet-read')(auth)

sheet_read.readFolder(folderId).then(...)

Here auth is the oauthclient with credential and token set on it.
See google drive quickstart for examples

readFolder resolves with data from multiple spreadsheets