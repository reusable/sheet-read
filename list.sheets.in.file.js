var bluebird = require('bluebird')
const { google } = require('googleapis');

module.exports = function (fileId, auth) {
    var D = bluebird.pending()

    //return bluebird.resolve(['sheet1', 'sheet2', 'sheet3'])
    const sheets = google.sheets({ version: 'v4', auth });

    sheets.spreadsheets.get({
        spreadsheetId: fileId
    }, (err, res) => {
        if (err) {
            console.log('The API returned an error: ' + err);
            D.reject(err)
        } else {
            //console.log('Sheets ', JSON.stringify(res.data.sheets))

            D.resolve(res.data.sheets)
        }


    })


    return D.promise
}