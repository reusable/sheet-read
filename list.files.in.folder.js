var bluebird = require('bluebird')
const { google } = require('googleapis');

module.exports = function (folderId, auth) {
    var D = bluebird.pending()

    /** Variable name change from auth to something else causes error */
    // const drive = google.drive({ version: 'v3', auth });
    // drive.files.list({
    //     'pageSize': 10,
    //     'fields': "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
    //     'q': "'" + folderId + "' in parents"
    // }, (err, res) => {
    //     if (err) return console.log('The API returned an error: ' + err);
    //     const files = res.data.files;
    //     console.log(files)
    // });

    const drive = google.drive({ version: 'v3', auth });
    //console.log('calling drive api to fetch files from folder ', folderId)
    drive.files.list({
        'pageSize': 10,
        'fields': "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
        'q': "'" + folderId + "' in parents"
    }, (err, res) => {
        if (err) {
            console.log('The Files List API returned an error: ', err);
            D.reject(err)
        } else {
            const files = res.data.files;
            //console.log(files)
            D.resolve(files)
        }

    });

    return D.promise
}